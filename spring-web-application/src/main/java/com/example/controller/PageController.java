package com.example.controller;

import java.util.ArrayList;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.domain.UiElement;

@Controller
public class PageController {

	@RequestMapping(path="/", method=RequestMethod.GET)
	public String testMessage(Model model){
		model.addAttribute("name", "Irina");
		UiElement el1= new UiElement("Ihr Name", "text");
		UiElement el2= new UiElement("Submit", "submit");
		UiElement el3= new UiElement("verheiratet", "checkbox");
		UiElement el5= new UiElement("Anzahl der Kinder", "number");
		UiElement el4= new UiElement("Anzahl der Kinder", "label");
		UiElement el6= new UiElement("verheiratet", "label");
		UiElement el7= new UiElement("Ihr Name", "label");
		UiElement el8= new UiElement("", "label");
		
		ArrayList<UiElement> elements = new ArrayList<UiElement>();
		
		elements.add(el1);
		
		elements.add(el5);
		
		elements.add(el3);
		elements.add(el8);
		elements.add(el2);
		
		model.addAttribute("elements", elements);
		return "web";
	}
	
}
